# -*- coding: utf-8 -*-
from keras.layers import Recurrent
from keras import activations, initializations, regularizers
from keras import backend as K
from keras.engine.topology import InputSpec 
import theano
import theano.tensor as T
class attentionRNN(Recurrent):
    '''Gated Recurrent Unit - Cho et al. 2014.

    # Arguments
        output_dim: dimension of the internal projections and the final output.
        init: weight initialization function.
            Can be the name of an existing function (str),
            or a Theano function (see: [initializations](../initializations.md)).
        inner_init: initialization function of the inner cells.
        activation: activation function.
            Can be the name of an existing function (str),
            or a Theano function (see: [activations](../activations.md)).
        inner_activation: activation function for the inner cells.
        W_regularizer: instance of [WeightRegularizer](../regularizers.md)
            (eg. L1 or L2 regularization), applied to the input weights matrices.
        U_regularizer: instance of [WeightRegularizer](../regularizers.md)
            (eg. L1 or L2 regularization), applied to the recurrent weights matrices.
        b_regularizer: instance of [WeightRegularizer](../regularizers.md),
            applied to the bias.
        dropout_W: float between 0 and 1. Fraction of the input units to drop for input gates.
        dropout_U: float between 0 and 1. Fraction of the input units to drop for recurrent connections.

    # References
        - [On the Properties of Neural Machine Translation: Encoder–Decoder Approaches](http://www.aclweb.org/anthology/W14-4012)
        - [Empirical Evaluation of Gated Recurrent Neural Networks on Sequence Modeling](http://arxiv.org/pdf/1412.3555v1.pdf)
        - [A Theoretically Grounded Application of Dropout in Recurrent Neural Networks](http://arxiv.org/abs/1512.05287)
    '''
    def __init__(self, output_dim,hidden_len,n_prime,hidden_input,
                 init='glorot_uniform', inner_init='orthogonal',
                 activation='tanh', inner_activation='hard_sigmoid',
                 W_regularizer=None, U_regularizer=None, b_regularizer=None,
                 AW_regularizer=None, Ab_regularizer=None,
                 dropout_W=0., dropout_U=0., **kwargs):
        self.output_dim = output_dim
        self.hidden_len = hidden_len
        self.hidden_input = hidden_input
        self.n_prime = n_prime
        self.init = initializations.get(init)
        self.inner_init = initializations.get(inner_init)
        self.activation = activations.get(activation)
        self.inner_activation = activations.get(inner_activation)
        self.W_regularizer = regularizers.get(W_regularizer)
        self.U_regularizer = regularizers.get(U_regularizer)
        self.b_regularizer = regularizers.get(b_regularizer)
        self.AW_regularizer = regularizers.get(AW_regularizer)
        self.Ab_regularizer = regularizers.get(Ab_regularizer)
        self.dropout_W, self.dropout_U = dropout_W, dropout_U

        if self.dropout_W or self.dropout_U:
            self.uses_learning_phase = True
        super(attentionRNN, self).__init__(**kwargs)

  
    def build(self, input_shape):
        self.input_spec = [InputSpec(shape=input_shape)]
        #input_dim = input_shape[2]
        input_dim = self.output_dim
        self.input_dim = self.output_dim
        print input_shape
        print input_dim
        print self.output_dim
        #self.output_dim = input_dim

        self.W_z = self.init((input_dim, self.output_dim),
                             name='{}_W_z'.format(self.name))
        self.U_z = self.inner_init((self.output_dim, self.output_dim),
                                   name='{}_U_z'.format(self.name))
        self.b_z = K.zeros((self.output_dim,), name='{}_b_z'.format(self.name))

        self.W_r = self.init((input_dim, self.output_dim),
                             name='{}_W_r'.format(self.name))
        self.U_r = self.inner_init((self.output_dim, self.output_dim),
                                   name='{}_U_r'.format(self.name))
        self.b_r = K.zeros((self.output_dim,), name='{}_b_r'.format(self.name))

        self.W_h = self.init((input_dim, self.output_dim),
                             name='{}_W_h'.format(self.name))
        self.U_h = self.inner_init((self.output_dim, self.output_dim),
                                   name='{}_U_h'.format(self.name))
        self.b_h = K.zeros((self.output_dim,), name='{}_b_h'.format(self.name))

        self.AW_1 = self.init((self.output_dim,self.n_prime),
                             name='{}_AW_1'.format(self.name))
        self.AW_2 = self.init((2*self.output_dim,self.n_prime),
                             name ='{}_AW_2'.format(self.name))
        self.AW_3 = self.init((self.n_prime,1),
                             name ='{}_AW_3'.format(self.name))
        self.C_z = self.init((2*self.output_dim,self.output_dim),
                             name ='{}_C_z'.format(self.name))
        self.C = self.init((2*self.output_dim,self.output_dim),
                             name ='{}_C'.format(self.name))
        self.C_r = self.init((2*self.output_dim,self.output_dim),
                             name ='{}_C_r'.format(self.name))
        self.regularizers = []
        if self.W_regularizer:
            self.W_regularizer.set_param(K.concatenate([self.W_z,
                                                        self.W_r,
                                                        self.W_h]))
            self.regularizers.append(self.W_regularizer)
        if self.U_regularizer:
            self.U_regularizer.set_param(K.concatenate([self.U_z,
                                                        self.U_r,
                                                        self.U_h]))
            self.regularizers.append(self.U_regularizer)
        if self.b_regularizer:
            self.b_regularizer.set_param(K.concatenate([self.b_z,
                                                        self.b_r,
                                                        self.b_h]))
            self.regularizers.append(self.b_regularizer)

        self.trainable_weights = [self.W_z, self.U_z, self.b_z,
                                  self.W_r, self.U_r, self.b_r,
                                  self.W_h, self.U_h, self.b_h,
                                  self.AW_1,self.AW_2,self.AW_3,
                                  self.C_z, self.C, self.C_r]
        if self.stateful:
            self.reset_states()
        else:
            # initial states: all-zero tensor of shape (output_dim)
            self.states = [None]

        if self.initial_weights is not None:
            self.set_weights(self.initial_weights)
            del self.initial_weights

    def reset_states(self):
        assert self.stateful, 'Layer must be stateful.'
        input_shape = self.input_spec[0].shape
        if not input_shape[0]:
            raise Exception('If a RNN is stateful, a complete ' +
                            'input_shape must be provided (including batch size).')
        if hasattr(self, 'states'):
            K.set_value(self.states[0],
                        np.zeros((input_shape[0], self.output_dim)))
        else:
            self.states = [K.zeros((input_shape[0], self.output_dim))]

    def preprocess_input(self, x):
    
        return x

    def step(self, x, states):
        h_tm1 = states[0]  # previous memory
        B_U = states[1]  # dropout matrices for recurrent units
        B_W = states[2]


        '''
        # out_dim = n
        # x size : batchedsize X ((hidden_len+1)*2+1)*output_dim
        
        hidden_len = self.hidden_len
        output_dim = self.output_dim

        #hidden_input size :  batchedsize X 2 * hidden_len * output_dim
        
        self.hidden_input = x[: , : 2*hidden_len*output_dim]
        
        #pre_hidden size : batchedsize X 2 * output_dim 
        
        self.pre_hidden = x[:,2*hidden_len*output_dim : 2*(hidden_len+1)*output_dim]

        #x size = batchedszie X output_dim
        
        x  = x[:,2*(hidden_len+1)*output_dim : 2*(hidden_len+1)*output_dim+output_dim]
        #print output_dim
        #print hidden_len
        #self.hidden_input size : batchedsize X 1 X 2 * hidden_len * output_dim
        self.hidden_input = self.hidden_input.dimshuffle((0,'x',1))

        #self.hidden_input size : batchedsize X hidden_len X 2 * output_dim
        self.hidden_input = K.concatenate([ self.hidden_input[:,:,i*2*output_dim:(i+1)*2*output_dim] for i in range(hidden_len)],1)

        #self.pre_hidden size : batchedsize X 1 X 2 * output_dim
        self.pre_hidden = self.pre_hidden.dimshuffle((0,'x',1))
        '''

        # h_tm1 size:ba X n
        # self.AW_1 size: n X n'    
        # eah size : ba X n' 
        eah = K.dot(h_tm1,self.AW_1)

        # self.hidden_input size : ba X hidden_len X 2n 
        # self.AW_2 size : 2n X n'
        # eas size : ba X hidden_len X n' 
        eas = K.dot(self.hidden_input,self.AW_2)

        # before dimshuffle eah size : ba X n'
        # after dimshuffle eah size : ba X 1 X n'
        # after concatenate eah size : ba X hidden_len X n'
        eah = K.concatenate([eah.dimshuffle((0,'x',1))]*self.hidden_len,1)


        # result of activation size :ba X hidden_len X n'
        # self.AW_3 size : n' X 1
        # e size : batched X hidden_len X 1
        e = K.dot(self.activation(eah + eas),self.AW_3)

        # before shuffle e size : batched X hidden_len X 1
        # after shuffle size : batched X hidden_len
        e = e.dimshuffle((2,0,1))[0]

        # alpha size: batched X hidden_size
        alpha = K.softmax(e)

        # alpha size :ba X 1 X hidden_size
        alpha = alpha.dimshuffle((0,'x',1))

        # self.hidden_input size : ba X hidden_len X 2n
        # alpha size : ba X 1 X hidden_len
        # c_i size : ba X 1 X 2n
        c_i = K.batch_dot(alpha,self.hidden_input)

        # c_i size : ba X 2n
        c_i = c_i.dimshuffle((1,0,2))[0]
        # x size : ba X n
        # self.W_z size : n X n
        # x_z size : ba X n
        x_z = K.dot(x * B_W[0], self.W_z) + self.b_z
        x_r = K.dot(x * B_W[1], self.W_r) + self.b_r
        x_h = K.dot(x * B_W[2], self.W_h) + self.b_h

        # x_z size : ba X n
        # h_tm1 size : ba X n
        # self.U_z size : n X n
        # z size : ba X n
        # c_i size : ba X 2n 
        # self.C_z size : 2n X n 
        z = self.inner_activation(x_z + K.dot(h_tm1 * B_U[0], self.U_z) + K.dot(c_i,self.C_z))
        r = self.inner_activation(x_r + K.dot(h_tm1 * B_U[1], self.U_r) + K.dot(c_i,self.C_r))

        # x_h size : ba X n
        # r size : ba X n
        # self.U_z size : n X n 
        # hh size : ba X n 
        hh = self.activation(x_h + K.dot(r * h_tm1 * B_U[2], self.U_h) + K.dot(c_i,self.C))
        
        # h size: ba X n
        h = z * h_tm1 + (1 - z) * hh
        return h, [h]

    def get_constants(self, x):
        constants = []
        if 0 < self.dropout_U < 1:
            ones = K.ones_like(K.reshape(x[:, 0, 0], (-1, 1)))
            ones = K.concatenate([ones] * self.output_dim, 1)
            B_U = [K.in_train_phase(K.dropout(ones, self.dropout_U), ones) for _ in range(3)]
            constants.append(B_U)
        else:
            constants.append([K.cast_to_floatx(1.) for _ in range(3)])

        if 0 < self.dropout_W < 1:
            input_shape = self.input_spec[0].shape
            input_dim = input_shape[-1]
            ones = K.ones_like(K.reshape(x[:, 0, 0], (-1, 1)))
            ones = K.concatenate([ones] * input_dim, 1)
            B_W = [K.in_train_phase(K.dropout(ones, self.dropout_W), ones) for _ in range(3)]
            constants.append(B_W)
        else:
            constants.append([K.cast_to_floatx(1.) for _ in range(3)])
        return constants

    def get_config(self):
        config = {"output_dim": self.output_dim,
                  "init": self.init.__name__,
                  "inner_init": self.inner_init.__name__,
                  "activation": self.activation.__name__,
                  "inner_activation": self.inner_activation.__name__,
                  "W_regularizer": self.W_regularizer.get_config() if self.W_regularizer else None,
                  "U_regularizer": self.U_regularizer.get_config() if self.U_regularizer else None,
                  "b_regularizer": self.b_regularizer.get_config() if self.b_regularizer else None,
                  "dropout_W": self.dropout_W,
                  "dropout_U": self.dropout_U}
        base_config = super(attentionRNN, self).get_config()
        return dict(list(base_config.items()) + list(config.items()))

