#from __future__ import print_function
from keras.models import Sequential
from keras.layers import Activation,Input,TimeDistributed,RepeatVector,\
recurrent,Embedding,merge,Dense,Flatten,Lambda
from keras.models import Model
import numpy as np
from six.moves import range
import sys
sys.path.append('./keras-1.0.2/keras/layers')
import random
import attention_Rnn  

aRNN = attention_Rnn.attentionRNN

config = {}
#The max number of sentence in article sets
config['max_nb_sentence'] = 3
#The max number of words in all sentences 
config['max_nb_words'] = 20
#vocabulary size of the training set
config['vocabulary_size']=20
#THe number of RNN attentional dense output
config['nb_RNN_output'] = 100
#The number of the dimensions of the word embedding
config['nb_dimen_word_embed'] =100
RNN = recurrent.SimpleRNN


def dimshuffle(x):
	return x.dimshuffle(0,'x',1)
def dimshuffle_output_shpae(input_shape):
	shape = list(input_shape)
	assert len(shape) == 2
	shape.append(shape[-1])
	shape[1] = 1
	return tuple(shape)
def createmodel(config):
	layerstack = []
	inputstack = []
	embedding = Embedding(config['vocabulary_size'], config['nb_dimen_word_embed'])
	forward1 = RNN(config['nb_RNN_output'])
	forward2 = RNN(config['nb_RNN_output'],go_backwards=True)
	for i in range(config['max_nb_sentence']):
		sequence = Input(shape=(config['max_nb_words'],),dtype='int32')
		inputstack.append(sequence)
		embedded = embedding(sequence)
		print embedded.ndim
		forward = forward1(embedded)
		backward = forward2(embedded)
		merged = merge([forward, backward], mode='concat', concat_axis=-1)
		la = Lambda(dimshuffle,output_shape = dimshuffle_output_shpae)(merged)
		layerstack.append(la)
	merged1 = merge(layerstack,mode='concat',concat_axis=1)
	print merged1.ndim
	forward2 = RNN(config['nb_RNN_output'],return_sequences=True)(merged1)
	backward2 = RNN(config['nb_RNN_output'],return_sequences=True,go_backwards=True)(merged1)
	merged2 =  merge([forward2, backward2], mode='concat', concat_axis=-1)
	
	inputY = Input(shape=(4,),dtype='int32')
	embededY = embedding(inputY)
	att = aRNN(100,3,200,merged2,return_sequences=True)(embededY)
	flattened  = Flatten()(att)
	inputstack.append(inputY)
	output = Dense(config['vocabulary_size'], activation='softmax')(flattened)
	model = Model(input=inputstack, output=output)
	model.compile('adam', 'binary_crossentropy', metrics=['accuracy'])
	return model
model = createmodel(config)
from keras.utils.visualize_util import plot
plot(model, to_file="model.png", show_shapes=True)

data1 = np.random.randint(0,19,(1000,20))
data2 = np.random.randint(0,19,(1000,4))
labels = np.zeros((1000,20))
for i in range(1000):
	labels[i, random.randint(0,19)] =1 
model.fit([data1,data1,data1,data2], labels,batch_size = 256)

