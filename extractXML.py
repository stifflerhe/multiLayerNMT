#coding=utf-8
import xml.etree.ElementTree as ET
tree = ET.ElementTree(file='data/eng')
root = tree.getroot()
X = open("data/context",'w')
Y = open("data/title",'w')
for DOC in root:
	headline = False
	context = ""
	for item in DOC :
		#print item.tag
		if item.tag == 'HEADLINE':
			head = item.text
			headline = True
		if item.tag ==  'TEXT':
			sentences = [ " ".join(i.text.strip().split("\n")) for i in item]
	#print sentences
	#print context
	if headline == True:
		X.write("、".join(sentences)+"\n")
		Y.write(head.strip()+"\n")

